const express = require('express');
const app = express();
const port = 4000;

app.use(express.static('public'));
app.set('view engine','ejs');

app.get('/',(req, res) => {
    res.render('home');
  });

  app.get('/contect',(req, res) => {
    res.render('contect');
  });

  app.get('/about',(req, res) => {
    res.render('about');
  });

  app.get('/services',(req, res) => {
    res.render('services')
  });

  app.get('/login',(req, res) => {
    res.render('login');
  });

app.listen(port, () =>{
    console.log(`server started on port ${port}`)
});